<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DLP_TITLE PROD</name>
   <tag></tag>
   <elementGuidId>eb591a05-88f8-481b-aa87-cc5a2d9b0a0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.product > div.card-body > h5.card-title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div[3]/div/div/h5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>2ecd363f-444c-47a6-a621-b149e5a07640</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-title</value>
      <webElementGuid>61de2720-ba48-445a-9d82-126d1e6760b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>baju okky</value>
      <webElementGuid>d430a6e5-2298-4cf0-953d-24ff142c211f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-4&quot;]/div[@class=&quot;card product&quot;]/div[@class=&quot;card-body&quot;]/h5[@class=&quot;card-title&quot;]</value>
      <webElementGuid>4fcbd12c-5758-4c3d-9f0f-13bb4c99cd69</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[3]/div/div/h5</value>
      <webElementGuid>46c83746-44a1-4edb-b364-aaea111ae15d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::h5[1]</value>
      <webElementGuid>6d406077-3c12-4f21-9823-0c31c6108d2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::h5[2]</value>
      <webElementGuid>2aa0c26b-c798-4510-81de-9b95d1a50a9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 400.000,00'])[7]/preceding::h5[1]</value>
      <webElementGuid>8056d20e-ad9a-4ea6-b2a2-9a6218d9d414</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hapus'])[1]/preceding::h5[1]</value>
      <webElementGuid>97920bf2-4c1d-4b33-9a07-2ec050ba0a2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/h5</value>
      <webElementGuid>db6bea24-583f-4f9c-ae9c-5730faa93eac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'baju okky' or . = 'baju okky')]</value>
      <webElementGuid>09ae9da1-d9a7-4de0-a8c7-6b330cc9b77e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
