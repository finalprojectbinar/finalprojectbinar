<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DLP_BTN Edit</name>
   <tag></tag>
   <elementGuidId>e2ec44b3-bb0d-4f67-86ec-ff4fc1d6fc79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-outline-primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div[3]/div/div/div/button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5dd3ccb2-dedc-4192-8c60-aff3f53cd5ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-outline-primary</value>
      <webElementGuid>b93d75a2-5c7c-4e34-a569-d9de8f63d11d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Edit</value>
      <webElementGuid>caae4b29-46db-41da-91c7-9afde8e4dc95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-4&quot;]/div[@class=&quot;card product&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;d-flex flex-column mt-4&quot;]/button[@class=&quot;btn btn-outline-primary&quot;]</value>
      <webElementGuid>63fc9351-b4f3-4f76-af91-69ad1630c6db</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div[3]/div/div/div/button[2]</value>
      <webElementGuid>370d7b7c-2b75-43e5-93f8-077ee92413fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hapus'])[1]/following::button[1]</value>
      <webElementGuid>86eb3c10-8b66-4770-8ad7-0a0f0f74b3ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 400.000,00'])[7]/following::button[2]</value>
      <webElementGuid>e6ff5202-bfd7-41e9-8290-39be7b158e4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='bocil kematian'])[1]/preceding::button[1]</value>
      <webElementGuid>f602ff95-b8fd-4cf8-80e1-0f543bb10875</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::button[1]</value>
      <webElementGuid>938c686e-96a6-4a05-bf8d-b7000e7721a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Edit']/parent::*</value>
      <webElementGuid>c6d2c122-07b9-47c2-8309-715432ca20c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]</value>
      <webElementGuid>dac38c10-b960-4326-99d2-fe7e2fbc3cfa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Edit' or . = 'Edit')]</value>
      <webElementGuid>36add410-d6f3-4724-8b64-ce05ca2a7250</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
