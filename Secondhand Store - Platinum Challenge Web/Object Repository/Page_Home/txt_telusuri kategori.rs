<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_telusuri kategori</name>
   <tag></tag>
   <elementGuidId>4e68c8df-a6ca-49ea-8129-c0e59da774df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.category > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>422f91c0-4b61-4bf4-9add-77587c2dff3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Telusuri Kategori</value>
      <webElementGuid>2f7bcc18-b7ca-44b2-a4c2-c045ffa53136</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;pb-5&quot;]/div[@class=&quot;container pb-5&quot;]/div[@class=&quot;category&quot;]/h3[1]</value>
      <webElementGuid>445a62d1-1693-45b6-98f4-ac1655bc49d9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/div/h3</value>
      <webElementGuid>8bcfa2bb-3a57-4527-ad1d-6e19d008fa48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga 60%'])[1]/following::h3[1]</value>
      <webElementGuid>62ac1e2c-dc26-40cd-846e-17d524be46bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulan Ramadan Banyak Diskon!'])[1]/following::h3[2]</value>
      <webElementGuid>5aee864d-0e79-48bf-ae1e-25f8b76ded13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Semua'])[1]/preceding::h3[1]</value>
      <webElementGuid>d9fc01df-be6c-47eb-8ce2-1b54fb7432f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Telusuri Kategori']/parent::*</value>
      <webElementGuid>fa162438-93b5-4e4e-87a3-bac9b0484b82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h3</value>
      <webElementGuid>4a45df92-0645-4278-af7c-da8ce5e2a21e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Telusuri Kategori' or . = 'Telusuri Kategori')]</value>
      <webElementGuid>22bef063-8f9c-489f-83ad-385b1c5d2356</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
