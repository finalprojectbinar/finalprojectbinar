<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EDP_BTN PROFIL</name>
   <tag></tag>
   <elementGuidId>9382814a-8edc-496d-8f5e-c28e18e10256</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#user</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='user']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>942b077c-fff8-4412-8aeb-e8d858dbb514</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user</value>
      <webElementGuid>98232ecc-4b4b-40ba-ae85-e15d0c4baa42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>66337896-6490-4895-9e4e-2060b693ed9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-link</value>
      <webElementGuid>ba160f04-2fa1-45b6-9f20-b8f36ad76305</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>0a80d442-b22d-4c57-aa7f-29f902af40f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-display</name>
      <type>Main</type>
      <value>static</value>
      <webElementGuid>d78c852f-c55d-4851-9349-f0ad1f8039eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>edeeb70f-8153-4427-a9f8-61487f20487f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user&quot;)</value>
      <webElementGuid>0984f57d-d5da-4e13-831a-72f614c0794d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='user']</value>
      <webElementGuid>14c0b043-5532-49b7-be87-f63c0fd82c58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarText']/ul/li[3]/button</value>
      <webElementGuid>22d97a17-70cd-443f-9490-44c5b3c2272e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ditawar Rp 1.000,00'])[3]/following::button[1]</value>
      <webElementGuid>43fd3fee-f310-41ef-8eb9-5d1789ef1442</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 400.000,00'])[6]/following::button[1]</value>
      <webElementGuid>a465e613-0f6b-499c-982d-8e37821a5e8b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profile'])[1]/preceding::button[1]</value>
      <webElementGuid>d5ac09a1-b1e1-4790-b133-fcb2ef92275a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log Out'])[1]/preceding::button[1]</value>
      <webElementGuid>d989859f-fa2a-413a-81db-79ca95945a38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/button</value>
      <webElementGuid>c49bf35a-ea50-4657-8aaf-844a018faa7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'user' and @type = 'button']</value>
      <webElementGuid>ec4603a1-fe7c-47c1-8e1b-7964f512f25f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
